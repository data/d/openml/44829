# OpenML dataset: cps88wages

https://www.openml.org/d/44829

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

This study uses data on males from the 1988 March CPS to sample the data. The March CPS contains information on previous year's wages, schooling, industry, and occupation. We select a sample of men ages 18 to 70 with positive annual income greater than 50 Dollars in 1992, who are not self-employed nor working without pay. The wage data is deflated by the deflator of Personal Consumption Expenditure for 1992. The data contains 28,155 observations and has variables characterizing the individuals.

The goal is to estimate the wage using information about working individuals.

**Attribute Description**

1. *wage* - target feature
2. *education* - years of schooling
3. *experience* - years of potential work experience
4. *ethnicity* - race ("cauc", "afam")
5. *smsa* - whether living in SMSA ("no", "yes")
6. *region* - living region ("northeast", "midwest", "south", "west")
7. *parttime* - whether working parttime ("no", "yes")

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44829) of an [OpenML dataset](https://www.openml.org/d/44829). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44829/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44829/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44829/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

